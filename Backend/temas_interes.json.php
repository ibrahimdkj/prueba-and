<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');

$tramitesMasUsados = array(
	array("id" => "1", "nombre"=>"Sabes que son los datos abiertos?", "breveDescripcion"=>"La información que producen las entidades públicas a tu alcance", "imagen"=>"assets/images/home/tema_interes_1.png",  "enlace"=>"https://enlace1.com","textoEnlace"=>"Me interesa!"),
	array("id" => "2", "nombre"=>"Conoce más sobre nuestro país", "breveDescripcion"=>"Colombia es hermosa, y queremos que conozcas más sobre ella", "imagen"=>"assets/images/home/tema_interes_2.png",  "enlace"=>"https://enlace2.com","textoEnlace"=>"Ir a Colombia.co!"),	
	array("id" => "3", "nombre"=>"Este portal está pensado para ti", "breveDescripcion"=>"GOV.CO nace para facilitarle a los ciudadanos la interacción con el Estado", "imagen"=>"assets/images/home/tema_interes_3.png",  "enlace"=>"https://enlace3.com","textoEnlace"=>"Quiero saber como funciona"),
	array("id" => "4", "nombre"=>"Destacado que esté en acondicionado", "breveDescripcion"=>"Texto descriptivo que capte la atención del lector", "imagen"=>"assets/images/home/tema_interes_4.png",  "enlace"=>"https://enlace4.com","textoEnlace"=>"Call to action")
	
	

);

echo json_encode($tramitesMasUsados); 
?>