<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');

$tramitesMasUsados = array(
	array("id" => "1", "nombre"=>"Legalización de documentos de educación superior para adelantar estudios o trabajar en el exterior", "entidad"=>"Ministerio de Educación", "lugarTramite"=>"online",  "costoTramite"=>"costo","enlaceTramite"=>"https://enlace1.com", "masUsado"=>"false"),
	array("id" => "2", "nombre"=>"Adopción de un niño,niña o adolescente por persona, cónyuges o compañeros permamentes residentes en Colombia", "entidad"=>"Instituto Colombiano de Bienestar Familiar - ICBF", "lugarTramite"=>"presencial",  "costoTramite"=>"gratis","enlaceTramite"=>"https://enlace1.com", "masUsado"=>"false"),
	array("id" => "3", "nombre"=>"Agendamiento de citas para consultorio jurídico", "entidad"=>"Ministerio de Justicia", "lugarTramite"=>"online",  "costoTramite"=>"gratis","enlaceTramite"=>"https://enlace3.com", "masUsado"=>"false"),
	array("id" => "2", "nombre"=>"Certificados y constancias académicas", "entidad"=>"Servicio Nacional de Aprendizaje - SENA", "lugarTramite"=>"online",  "costoTramite"=>"gratis","enlaceTramite"=>"https://enlace4.com", "masUsado"=>"false")
	

);

echo json_encode($tramitesMasUsados); 
?>