import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GovFooterComponent } from './components/gov-footer/gov-footer.component';
import { GovHeaderComponent } from './components/gov-header/gov-header.component';
import { GovMensajeSuperiorComponent } from './components/gov-mensaje-superior/gov-mensaje-superior.component';

import { GovSlidePrincipalComponent } from './components/gov-slide-principal/gov-slide-principal.component';
import { GovCardPrincipalComponent } from './components/gov-card-principal/gov-card-principal.component';
import { GovTramitesMasUsadosComponent } from './components/gov-tramites-mas-usados/gov-tramites-mas-usados.component';
import { GovOtrosTemasInteresComponent } from './components/gov-otros-temas-interes/gov-otros-temas-interes.component';

import{ HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    GovFooterComponent,
    GovHeaderComponent,
    GovMensajeSuperiorComponent,
    GovSlidePrincipalComponent,
    GovCardPrincipalComponent,
    GovTramitesMasUsadosComponent,
    GovOtrosTemasInteresComponent

   
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
