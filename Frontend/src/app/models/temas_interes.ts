export class TemasInteres{
    constructor(
        public _id:Int16Array,
        public _nombre:string,
        public _breveDescripcion:string,
        public _imagen:string,
        public _enlace:string,
        public _textoEnlace:string
    ){}
}