export class TramitesMasUsados{
    constructor(
        public _id:Int16Array,
        public _nombre:string,
        public _entidad:string,
        public _lugarTramite:string,
        public _costoTramite:string,
        public _masUsado:boolean
    ){}
}