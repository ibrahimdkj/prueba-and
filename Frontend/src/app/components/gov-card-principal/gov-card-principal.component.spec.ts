import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GovCardPrincipalComponent } from './gov-card-principal.component';

describe('GovCardPrincipalComponent', () => {
  let component: GovCardPrincipalComponent;
  let fixture: ComponentFixture<GovCardPrincipalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GovCardPrincipalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GovCardPrincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
