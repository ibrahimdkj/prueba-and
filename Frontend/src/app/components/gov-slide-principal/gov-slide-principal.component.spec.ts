import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GovSlidePrincipalComponent } from './gov-slide-principal.component';

describe('GovSlidePrincipalComponent', () => {
  let component: GovSlidePrincipalComponent;
  let fixture: ComponentFixture<GovSlidePrincipalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GovSlidePrincipalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GovSlidePrincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
