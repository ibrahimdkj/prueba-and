import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gov-mensaje-superior',
  templateUrl: './gov-mensaje-superior.component.html',
  styleUrls: ['./gov-mensaje-superior.component.css']
})
export class GovMensajeSuperiorComponent implements OnInit {

  show=true;
  mensaje="Estamos construyendo este espacio para ti y tu opinión nos ayuda a crecer.";
  constructor() { }

  ngOnInit(): void {
  }

  mostrarNotificacion(){
    this.show=true;
  }

  cerrarNotificacion(){
    this.show=false;
  }

}
