import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GovMensajeSuperiorComponent } from './gov-mensaje-superior.component';

describe('GovMensajeSuperiorComponent', () => {
  let component: GovMensajeSuperiorComponent;
  let fixture: ComponentFixture<GovMensajeSuperiorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GovMensajeSuperiorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GovMensajeSuperiorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
