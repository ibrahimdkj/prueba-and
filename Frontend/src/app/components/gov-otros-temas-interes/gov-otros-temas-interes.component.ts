import { Component, OnInit } from '@angular/core';
import{ TemasInteres} from '../../models/temas_interes';
import {TemasInteresService} from '../../services/temas_interes.service';

@Component({
  selector: 'app-gov-otros-temas-interes',
  templateUrl: './gov-otros-temas-interes.component.html',
  styleUrls: ['./gov-otros-temas-interes.component.css'],
  providers:[TemasInteresService]
})
export class GovOtrosTemasInteresComponent implements OnInit {
  public temasInteres:TemasInteres[];
  constructor(private _temasInteresService:TemasInteresService) { }

  ngOnInit(): void {
    
    this._temasInteresService.getTemasInteres().subscribe(
      response=>{        
          this.temasInteres=response;   
      },
      error=>{
        console.log(error);
      }
    );
    
    
  }

}
