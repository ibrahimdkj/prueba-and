import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GovOtrosTemasInteresComponent } from './gov-otros-temas-interes.component';

describe('GovOtrosTemasInteresComponent', () => {
  let component: GovOtrosTemasInteresComponent;
  let fixture: ComponentFixture<GovOtrosTemasInteresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GovOtrosTemasInteresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GovOtrosTemasInteresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
