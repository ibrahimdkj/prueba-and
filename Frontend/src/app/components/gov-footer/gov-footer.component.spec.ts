import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GovFooterComponent } from './gov-footer.component';

describe('GovFooterComponent', () => {
  let component: GovFooterComponent;
  let fixture: ComponentFixture<GovFooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GovFooterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GovFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
