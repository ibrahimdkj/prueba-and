import { Component, OnInit } from '@angular/core';
import {TramitesMasUsadosService} from '../../services/tramites_mas_usados.service';
import{ TramitesMasUsados} from '../../models/tramites_mas_usados';


@Component({
  selector: 'app-gov-tramites-mas-usados',
  templateUrl: './gov-tramites-mas-usados.component.html',
  styleUrls: ['./gov-tramites-mas-usados.component.css'],
  providers:[TramitesMasUsadosService]
})
export class GovTramitesMasUsadosComponent implements OnInit {
  public tramitesMasUsados:TramitesMasUsados[];
  

  constructor( private _tramitesService:TramitesMasUsadosService) { 

  }

  ngOnInit(): void {
    this._tramitesService.getTramitesMasUsados().subscribe(
      response=>{
        
          this.tramitesMasUsados=response;
          console.log(this.tramitesMasUsados);
      
      },
      error=>{
        console.log(error);
      }
    );
    
  }

}
