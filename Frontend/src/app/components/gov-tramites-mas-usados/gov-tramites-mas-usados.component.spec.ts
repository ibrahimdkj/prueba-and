import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GovTramitesMasUsadosComponent } from './gov-tramites-mas-usados.component';

describe('GovTramitesMasUsadosComponent', () => {
  let component: GovTramitesMasUsadosComponent;
  let fixture: ComponentFixture<GovTramitesMasUsadosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GovTramitesMasUsadosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GovTramitesMasUsadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
