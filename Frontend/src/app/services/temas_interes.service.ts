import { Injectable } from '@angular/core';
import{ TemasInteres} from '../models/temas_interes';
import{ HttpClient, HttpHeaders} from '@angular/common/http';
import{ Observable} from 'rxjs';
import{ Global } from './global';

@Injectable()
export class TemasInteresService{
    public temasInteres:any;
    public url:string;
   
    constructor(
        private _http:HttpClient
    ){
        this.url=Global.url;

    }
    getTemasInteres():Observable<any>{
        return this._http.get(this.url+'temas_interes.json.php');
    }

}