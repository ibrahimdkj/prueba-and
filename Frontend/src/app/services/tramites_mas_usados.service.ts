import { Injectable } from '@angular/core';
import{ TramitesMasUsados} from '../models/tramites_mas_usados';
import{ HttpClient, HttpHeaders} from '@angular/common/http';
import{ Observable} from 'rxjs';
import{ Global } from './global';

@Injectable()
export class TramitesMasUsadosService{
    public tramitesMasUsados:TramitesMasUsados;
    public url:string;

    constructor(
        private _http:HttpClient
    ){
        this.url=Global.url;

    }

    getTramitesMasUsados():Observable<any>{
        return this._http.get(this.url+'tramites_mas_usados.json.php');
    }

}